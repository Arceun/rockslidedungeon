﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Settings {

    // SOUND
    public const bool default_mute = false;
    static public bool mute = false;

    public const float default_master = 1f;
    static public float master = 1f;

    public const float default_SFX = 0.25f;
    static public float SFX = 0.25f;

    public const float default_music = 0.5f;
    static public float music = 0.5f;

    // GAME

    public const float default_sensitivity = 5f;
    static public float sensitivity = 5f;

    public const float default_smoothing = 2f;
    static public float smoothing = 2f;
}
