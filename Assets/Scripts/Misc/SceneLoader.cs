﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SceneLoader {

    public static int START_MENU = 0;
    public static int DEMO_SCENE = 1;
    public static int LOADING_SCENE = 2;


    private static int previous_scene;

    public static void loadScene(int scene) {
        previous_scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene);
    }

    public static void loadPreviousScene() {
        SceneManager.LoadScene(previous_scene);
    }

}
