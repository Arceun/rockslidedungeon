﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.Collections;

public class GameManager : MonoBehaviour {


	public Maze mazePrefab;
	public NavMeshSurface navMesh;
	public float scale;
    public Parchment story;

    public Text nbGemDisp;
    public int nbGem = 0;

    public GameObject gem;

    public int enemyKilled = 0;
    public int killToGem = 5;
    public bool gemSpawned = false;

    public List<GameObject> enemies = new List<GameObject>();
    public PlayerManager player;

    public List<GameObject> enemiesGO = new List<GameObject>();
    public GameObject enemiesFolder;

    public List<GameObject> chests = new List<GameObject>();

    public AudioSource source;
    public AudioClip rock;

    private Maze mazeInstance;
    private Animator anim;

    private KeyCode[] cheatVision = { KeyCode.UpArrow, KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.B, KeyCode.A, KeyCode.Return};
    private int indexCheat = 0;

    public OptionsMenu optionsDisplayer;

    public Compass compass;

    void Start () {
        AudioSource[] sounds = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource pitch in sounds) {
            pitch.volume = (Settings.mute ? 0f : Settings.master * Settings.SFX);
        }
        Camera.main.gameObject.GetComponent<AudioSource>().volume = (Settings.mute ? 0f : Settings.master * Settings.music);
        BeginGame();
        new WaitForSeconds(3f);
        spawnEnemies();
    }

    void Update() {
        AudioSource[] sounds = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource pitch in sounds) {
            pitch.volume = (Settings.mute ? 0f : Settings.master * Settings.SFX);
        }
        foreach (GameObject chest in chests) {
            if (chest != null && chest.GetComponent<Chest>().isDeleted)
                Destroy(chest);
        }

        Camera.main.gameObject.GetComponent<AudioSource>().volume = (Settings.mute ? 0f : Settings.master * Settings.music);
        if (!story.gameObject.activeSelf) {
            if (Input.GetKeyDown(KeyCode.F7)) {
                RestartGame();
            }
            if (Input.GetKeyDown(KeyCode.F8)) {
                if (Time.timeScale == 0) {
                    optionsDisplayer.gameObject.SetActive(false);
                    Cursor.lockState = CursorLockMode.Locked;
                    Time.timeScale = 1;
                } else {
                    Time.timeScale = 0;
                    Cursor.lockState = CursorLockMode.None;
                    optionsDisplayer.gameObject.SetActive(true);
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape)) {
                SceneWaitingLoader.scene = SceneLoader.START_MENU;
                SceneLoader.loadScene(SceneLoader.LOADING_SCENE);
            }
            if (Input.anyKeyDown) {
                if (Input.GetKeyDown(cheatVision[indexCheat])) {
                    indexCheat++;
                } else {
                    indexCheat = 0;
                }
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
                Cursor.lockState = CursorLockMode.None;
            else if (Input.GetKeyUp(KeyCode.LeftControl))
                Cursor.lockState = CursorLockMode.Locked;

            if (indexCheat == cheatVision.Length) {
                player.toggleCheat();
                indexCheat = 0;
            }

            if (!gemSpawned && enemyKilled >= killToGem) {
                popGem();
            }
        }
    }

    private void BeginGame() {
		mazeInstance = Instantiate(mazePrefab) as Maze;
        mazeInstance.GM = this;
		mazeInstance.Generate();
		mazeInstance.transform.localScale = new Vector3(scale, scale, scale);
		mazeInstance.GetComponent<NavMeshSurface>().BuildNavMesh();
        GameObject chest = chests[UnityEngine.Random.Range(0, chests.Count)];
        Vector3 spawnPos = chest.transform.position;
        spawnPos.y = player.gameObject.transform.position.y;
        player.gameObject.transform.position = spawnPos;
        Camera.main.gameObject.GetComponent<CamMouseLook>().rotationAtStart = chest.transform.localRotation;
        chest.GetComponent<Chest>().isDeleted = true;
    }

    private void spawnEnemies() {
        for (int i = 0; i < 10; i++) {
            int enemySpawned = UnityEngine.Random.Range(0, enemiesGO.Count);
            int cellX = UnityEngine.Random.Range(0, mazePrefab.size.x);
            int cellY = UnityEngine.Random.Range(0, mazePrefab.size.z);
            GameObject enemy = Instantiate(enemiesGO[enemySpawned], mazeInstance.GetCell(new IntVector2(cellX, cellY)).transform.position, Quaternion.identity);
            enemy.GetComponent<NavMeshAgent>().Warp(mazeInstance.GetCell(new IntVector2(cellX, cellY)).transform.position);
            enemy.transform.SetParent(enemiesFolder.transform);
            enemies.Add(enemy);
        }
    }

	private void RestartGame() {
        player.disableTorch();
        source.PlayOneShot(rock);
        StartCoroutine(displayerLoader());
        Time.timeScale = 0f;
        Destroy(mazeInstance.gameObject);
        foreach (GameObject GO in enemies) {
            DestroyObject(GO);
        }
        enemies.Clear();
        foreach (GameObject GO in chests) {
            DestroyObject(GO);
        }
        chests.Clear();
        gemSpawned = false;
        enemyKilled = 0;
        player.playerStats.HP = player.playerStats.MaxHP;
        player.playerStats.Stam = player.playerStats.MaxStam;
        player.playerStats.Damage = player.playerStats.baseDamage * 1.25f;
        compass.resetNeedleAndCount();
        BeginGame();
        Time.timeScale = 1f;
        Camera.main.gameObject.GetComponent<CamMouseLook>().replaceCam();
    }

    private IEnumerator displayerLoader() {
        yield return new WaitForSeconds(7f);
        player.enableTorch();
        yield return new WaitForSeconds(3f);
        spawnEnemies();
    }

    public Maze getMaze() {
        return mazeInstance;
    }

    public void foundGem() {
        nbGem++;
        nbGemDisp.text = nbGem.ToString();
        RestartGame();
    }

    public void popGem() {
        gemSpawned = true;
        int cellX = UnityEngine.Random.Range(0, mazePrefab.size.x);
        int cellY = UnityEngine.Random.Range(0, mazePrefab.size.z);
        Vector3 SpawnPos = mazeInstance.GetCell(new IntVector2(cellX, cellY)).transform.position;
        SpawnPos.y = 165;
        Instantiate(gem, SpawnPos, Quaternion.identity);
        SpawnPos.y = 0;
        compass.positionGem = SpawnPos;
        compass.displayNeedle();
        Debug.Log("Gem has popped");
    }
}
