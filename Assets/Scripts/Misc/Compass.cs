﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour {

    public GameManager GM;

    public Vector3 positionGem;

    public Image needle;
    public Text toKill;

    private bool needlesDisp = false;

    private void Start() {
        toKill.text = (GM.killToGem - GM.enemyKilled).ToString();
        toKill.gameObject.SetActive(true);
        needle.gameObject.SetActive(false);
    }

    private void Update() {
        if (needlesDisp) {
            Vector3 forw = GM.player.transform.forward;
            forw.y = 0;
            Vector3 pos = GM.player.transform.position;
            pos.y = 0;
            needle.gameObject.transform.rotation = Quaternion.Euler(0, 0, -Vector3.SignedAngle(forw, positionGem - pos, Vector3.up));
        } else {
            toKill.text = (GM.killToGem - GM.enemyKilled).ToString();
        }
    }

    public void displayNeedle() {
        toKill.gameObject.SetActive(false);
        needle.gameObject.SetActive(true);
        needlesDisp = true;
    }

    public void resetNeedleAndCount() {
        toKill.text = (GM.killToGem - GM.enemyKilled).ToString();
        toKill.gameObject.SetActive(true);
        needle.gameObject.SetActive(false);
        needlesDisp = false;
    }

}
