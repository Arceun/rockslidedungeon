﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneWaitingLoader : MonoBehaviour {

    static public int scene;

    private bool loadScene = false;
    public Text loadingText;
    public float speed;


    void Update() {
        if (loadScene == false) {
            loadScene = true;
            loadingText.text = "Loading...";
            StartCoroutine(LoadNewScene());

        }
        if (loadScene == true) {
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, (Mathf.Sin(Time.time * speed) + 1.0f) / 2f);
        }
    }

    IEnumerator LoadNewScene() {
        yield return new WaitForSeconds(5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        while (!async.isDone) {
            yield return null;
        }
    }
}
