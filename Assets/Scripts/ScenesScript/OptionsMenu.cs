﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour {

    public MainMenu menu;

    public Toggle muteSound;
    public Slider masterSound;
    public Slider SFXSound;
    public Slider musicSound;
    public Slider sensitivityMouse;
    public Slider smoothingMouse;

    private void Start() {
        muteSound.isOn = Settings.mute;
        masterSound.value = Settings.master;
        SFXSound.value = Settings.SFX;
        musicSound.value = Settings.music;
        sensitivityMouse.value = Settings.sensitivity;
        smoothingMouse.value = Settings.smoothing;
    }

    public void applyChanges() {
        Settings.mute = muteSound.isOn;
        Settings.master = masterSound.value;
        Settings.SFX = SFXSound.value;
        Settings.music = musicSound.value;
        Settings.sensitivity = sensitivityMouse.value;
        Settings.smoothing = smoothingMouse.value;
    }

    public void resetChanges() {
        Settings.mute = Settings.default_mute;
        muteSound.isOn = Settings.mute;
        Settings.master = Settings.default_master;
        masterSound.value = Settings.master;
        Settings.SFX = Settings.default_SFX;
        SFXSound.value = Settings.SFX;
        Settings.music = Settings.default_music;
        musicSound.value = Settings.music;
        Settings.sensitivity = Settings.default_sensitivity;
        sensitivityMouse.value = Settings.sensitivity;
        Settings.smoothing = Settings.default_smoothing;
        smoothingMouse.value = Settings.smoothing;
    }

    public void returnButton() {
        gameObject.SetActive(false);
        menu.baseButton.SetActive(true);
    }
}
