﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour {

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneLoader.loadScene(SceneLoader.START_MENU);
        }
    }

    public void finishCredits() {
        transform.parent.gameObject.SetActive(false);
    }

}
