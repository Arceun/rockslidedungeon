﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject credits;
    public GameObject options;
    public GameObject baseButton;

    private void Start() {
        Cursor.lockState = CursorLockMode.None;
    }

    public void startGame() {
        SceneWaitingLoader.scene = SceneLoader.DEMO_SCENE;
        SceneLoader.loadScene(SceneLoader.LOADING_SCENE);
    }

    public void manageOptions() {
        baseButton.SetActive(false);
        options.SetActive(true);
    }

    public void viewCredits() {
        credits.SetActive(true);
    }

    public void quitGame() {
        Application.Quit();
    }
}
