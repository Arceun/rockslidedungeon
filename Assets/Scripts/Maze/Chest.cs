﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    public Animator anim;
    public bool isOpen = false;
    static public int[] lootTable = { 25, 50, 65, 85, 95, 100 };

    public List<GameObject> loots = new List<GameObject>();

    public AudioSource source;
    public AudioClip openning;

    public bool isDeleted = false;

    private void Start() {
        anim = GetComponentInChildren<Animator>();
    }

    public void openChest() {
        source.PlayOneShot(openning);
        anim.SetBool("open", true);
        isOpen = true;
        StartCoroutine(lootItem());
    }

    public IEnumerator lootItem() {
        yield return new WaitForSeconds(1f);
        int loot = Random.Range(0, 100);
        for (int i = 0; i < lootTable.Length; i++) {
            if (loot <= lootTable[i]) {
                instantiateItem((LootableObject.TypeObject)i);
                yield break;
            }
        }
    }

    public void instantiateItem(LootableObject.TypeObject type) {
        Vector3 SpawnPos = transform.position;
        SpawnPos.y = 165;
        switch (type) {
            case LootableObject.TypeObject.LIFE:
                Instantiate(loots[0], SpawnPos, Quaternion.identity);
                break;
            case LootableObject.TypeObject.STAMINA:
                Instantiate(loots[1], SpawnPos, Quaternion.identity);
                break;
            case LootableObject.TypeObject.MAXLIFE:
                Instantiate(loots[2], SpawnPos, Quaternion.identity);
                break;
            case LootableObject.TypeObject.MAXSTAMINA:
                Instantiate(loots[3], SpawnPos, Quaternion.identity);
                break;
            case LootableObject.TypeObject.BERZERK:
                Instantiate(loots[4], SpawnPos, Quaternion.identity);
                break;
            case LootableObject.TypeObject.INVISIBLE:
                Instantiate(loots[5], SpawnPos, Quaternion.identity);
                break;
        }
    }

}