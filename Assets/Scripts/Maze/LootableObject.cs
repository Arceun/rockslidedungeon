﻿using System.Collections.Generic;
using UnityEngine;

public class LootableObject : MonoBehaviour {

    public enum TypeObject {
        LIFE,
        STAMINA,
        MAXLIFE,
        MAXSTAMINA,
        BERZERK,
        INVISIBLE,
        GEM
    }

    public TypeObject type;

    public GameManager GM;
    public bool looted = false;

    private void Start() {
        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if (type == TypeObject.GEM)
            gameObject.GetComponentInParent<AudioSource>().spatialBlend = 1;
    }

    private void Update() {
        if (looted)
            Destroy(gameObject);
    }

    public void lootItem() {
        if (type != TypeObject.GEM) {
            GM.player.inventory.addItem(type);
        } else {
            GM.foundGem();
        }
        looted = true;
    }

}