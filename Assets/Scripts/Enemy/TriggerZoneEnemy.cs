﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZoneEnemy : MonoBehaviour {

    public PlayerManager targets = null;

	// Use this for initialization
	void Start () {
	}

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            PlayerManager go = other.GetComponent<PlayerManager>();
            targets = go;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")) {
            targets = null;
        }
    }
}
