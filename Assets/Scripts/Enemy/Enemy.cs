﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    public enum Type {
        GOBLIN,
        HOBGOBLIN,
        WOLF,
        TROLL
    }

    public enum State {
        PATROL,
        ATTACK,
        CHASING,
        DEAD,
        LOOKAT,
        NONE
    }

    public Type typeMob;
    public float life;
    public float dmg;
    public uint speed;
    public uint angularSpeed;
    public uint acceleration;
    public uint visionRadius;

    public bool dead = false;

    public Animator anim;
    public TriggerZoneEnemy triggerAttack;
    public GameManager GM;
    public AudioSource source;

    // Troll
    public AudioClip patrolTroll;
    public AudioClip attackTroll;
    public AudioClip deathTroll;

    // Wolf
    public AudioClip patrolWolf;
    public AudioClip attackWolf;
    public AudioClip deathWolf;


    // Goblin
    public AudioClip patrolGoblin;
    public AudioClip attackGoblin;
    public AudioClip deathGoblin;

    private AudioClip patrol;
    private AudioClip attack;
    private AudioClip death;
    private NavMeshAgent nav;
    private NavMeshPath path;
    private State actualState;
    private float elapsed = 0.0f;
    private bool isAttacking = false;
    private bool isInPatrol = false;

    // Use this for initialization
    void Start() {
        changeState(State.NONE);
        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        anim = GetComponent<Animator>();
        if (anim == null) {
            anim = GetComponent<Animator>();
        }
        nav = GetComponent<NavMeshAgent>();
        nav.isStopped = false;
        path = new NavMeshPath();
        elapsed = 0.0f;
        float multiplier = 1 + (GM.nbGem / 2);
        switch (typeMob) {
            case Type.GOBLIN:
                life = 40 * multiplier;
                dmg = 20 * multiplier;
                speed = 270;
                angularSpeed = 250;
                acceleration = 200;
                visionRadius = 7000;
                patrol = patrolGoblin;
                death = deathGoblin;
                attack = attackGoblin;
                break;
            case Type.HOBGOBLIN:
                life = 100 * multiplier;
                dmg = 15 * multiplier;
                speed = 230;
                angularSpeed = 250;
                acceleration = 200;
                visionRadius = 5000;
                patrol = patrolGoblin;
                death = deathGoblin;
                attack = attackGoblin;
                break;
            case Type.WOLF:
                life = 20 * multiplier;
                dmg = 15 * multiplier;
                speed = 400;
                angularSpeed = 700;
                acceleration = 500;
                visionRadius = 9500;
                patrol = patrolWolf;
                death = deathWolf;
                attack = attackWolf;
                break;
            case Type.TROLL:
                life = 250 * multiplier;
                dmg = 40 * multiplier;
                speed = 180;
                angularSpeed = 250;
                acceleration = 200;
                visionRadius = 3500;
                patrol = patrolTroll;
                death = deathTroll;
                attack = attackTroll;
                break;
        }
        nav.speed = speed;
        nav.angularSpeed = angularSpeed;
        nav.acceleration = acceleration;
        source.clip = patrol;
        source.spatialBlend = 1f;
        source.Play();
    }
	
	// Update is called once per frame
	void Update () {

        if (!dead && life <= 0) {
            setToDeath();
            return;
        }

        if (actualState == State.DEAD)
            return;
        if (anim == null) {
            anim = GetComponent<Animator>();
        }

        checkInRadius();

        if (isInPatrol) {
            for (int i = 0; i < path.corners.Length - 1; i++)
                Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.blue);
            if (nav.remainingDistance <= nav.stoppingDistance)
                isInPatrol = false;
        }

        if (actualState == State.CHASING) {
            Vector3 dest = GM.player.transform.position;
            dest.y = 0;
            NavMesh.CalculatePath(transform.position, dest, NavMesh.AllAreas, path);
            nav.isStopped = false;
            elapsed += Time.deltaTime;
            if (elapsed > 0.5f) {
                elapsed -= 0.5f;
                nav.SetPath(path);
            }
            for (int i = 0; i < path.corners.Length - 1; i++) {
                Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
            }
            anim.SetBool("run", true);
            anim.SetBool("attack", false);
        } else if (actualState == State.LOOKAT) {
            Vector3 direction = GM.player.transform.position - transform.position;
            direction.y = 0;
            this.transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(direction), 0.1f);
        } else if (actualState == State.ATTACK) {
            anim.SetBool("attack", true);
            anim.SetBool("run", false);
            if (!isAttacking) soundAttack();
            nav.isStopped = true;
            isAttacking = true;
        } else if (!isInPatrol && actualState == State.PATROL) {
            int cellX = UnityEngine.Random.Range(0, GM.mazePrefab.size.x);
            int cellY = UnityEngine.Random.Range(0, GM.mazePrefab.size.z);
            nav.isStopped = false;
            Vector3 dest = GM.getMaze().GetCell(new IntVector2(cellX, cellY)).transform.position;
            dest.y = 0;
            NavMesh.CalculatePath(transform.position, dest, NavMesh.AllAreas, path);
            nav.SetPath(path);
            anim.SetBool("run", true);
            anim.SetBool("attack", false);
            isInPatrol = true;
        }
    }

    public void soundAttack() {
        source.PlayOneShot(attack);
    }

    private float GetPathLength(NavMeshPath path) {
        float lng = 0.0f;

        if (path.status != NavMeshPathStatus.PathInvalid) {
            for (int i = 1; i < path.corners.Length; ++i) {
                lng += Vector3.Distance(path.corners[i - 1], path.corners[i]);
            }
        }

        return lng;
    }

    public void attackEnemy() {
        if (triggerAttack.targets != null) { 
            GM.player.takeDamage(dmg, transform);
        }
    }

    public void resetAttackingState() {
        isAttacking = false;
    }

    private void checkInRadius() {
        NavMeshPath tmp = new NavMeshPath();
        Vector3 dest = GM.player.transform.position;
        dest.y = 0;
        NavMesh.CalculatePath(transform.position, dest, NavMesh.AllAreas, tmp);

        if (tmp.status != NavMeshPathStatus.PathInvalid && !dead && !GM.player.isDead && GM.player.playerStats.isVisible && GetPathLength(tmp) < visionRadius) {
            if (GetPathLength(tmp) > nav.stoppingDistance && !isAttacking) {
                changeState(State.CHASING);
            } else if (!isAttacking) {
                float angle = 50;
                Vector3 forw = transform.forward;
                forw.y = 0;
                Vector3 playPos = GM.player.transform.position;
                playPos.y = 0;
                Vector3 pos = transform.position;
                pos.y = 0;
                if (Vector3.Angle(forw, playPos - pos) > angle) {
                    changeState(State.LOOKAT);
                } else {
                    changeState(State.ATTACK);
                }
            }
        } else {
            changeState(State.PATROL);
        }
        if (GM.player.isDead) {
            changeState(State.PATROL);
            isInPatrol = false;
        }

        if (tmp.status == NavMeshPathStatus.PathInvalid)
            isInPatrol = false;
    }

    private void changeState(State newState) {
        actualState = newState;
    }

    public void takeDamage(float dmg) {
        life -= dmg;
    }

    private void setToDeath() {
        dead = true;
        source.loop = false;
        source.PlayOneShot(death);
        if (anim == null) {
            anim = GetComponent<Animator>();
        }
        anim.SetBool("dead", true);
        GM.enemyKilled += 1;
        GetComponent<Rigidbody>().detectCollisions = false;
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        changeState(State.DEAD);
        StartCoroutine(removeEnemyFromMaze());
    }

    private IEnumerator removeEnemyFromMaze() {
        yield return new WaitForSeconds(1f);
        GetComponent<Rigidbody>().drag = 0f;
        GetComponent<Rigidbody>().angularDrag = 0f;
        yield return new WaitForSeconds(5f);
        GetComponent<Rigidbody>().isKinematic = true;
        Destroy(gameObject);
    }
}
