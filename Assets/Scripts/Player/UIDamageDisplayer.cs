﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDamageDisplayer : MonoBehaviour {

    public Image deadScreen;
    public Text scoreDisplay;

    public Image frontDamage;
    public Image leftDamage;
    public Image backDamage;
    public Image rightDamage;

    public Image berzerkMode;
    public Image invisibleMode;

    private void Update() {
        
    }

    public void displayDead(int score) {
        deadScreen.gameObject.SetActive(true);
        scoreDisplay.text = "You are dead!!\nScore : " + score.ToString();
        scoreDisplay.gameObject.SetActive(true);
    }

    public IEnumerator displayFront() {
        frontDamage.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        frontDamage.gameObject.SetActive(false);
    }

    public IEnumerator displayLeft() {
        leftDamage.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        leftDamage.gameObject.SetActive(false);
    }

    public IEnumerator displayBack() {
        backDamage.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        backDamage.gameObject.SetActive(false);
    }

    public IEnumerator displayRight() {
        rightDamage.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        rightDamage.gameObject.SetActive(false);
    }
}
