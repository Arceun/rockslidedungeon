﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

    public PlayerManager player;

    public List<GameObject> objects = new List<GameObject>();
    public List<Image> CDDisp = new List<Image>();
    public List<Text> nbObjDisp = new List<Text>();

    private int nbObject = 6;
    public float[] baseCDObjects = new float[6];
    public float[] timerObjects = new float[6];
    public int[] nbObjects = new int[6];

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

        for (int i = 0; i < nbObject; i++) {
            timerObjects[i] -= Time.deltaTime;
            timerObjects[i] = Mathf.Clamp(timerObjects[i], 0f, 60f);
            CDDisp[i].fillAmount = timerObjects[i] / baseCDObjects[i];
            nbObjDisp[i].text = nbObjects[i].ToString();
        }


        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            useItem(LootableObject.TypeObject.LIFE);
        } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
            useItem(LootableObject.TypeObject.STAMINA);
        } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
            useItem(LootableObject.TypeObject.MAXLIFE);
        } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
            useItem(LootableObject.TypeObject.MAXSTAMINA);
        } else if (Input.GetKeyDown(KeyCode.Alpha5)) {
            useItem(LootableObject.TypeObject.BERZERK);
        } else if (Input.GetKeyDown(KeyCode.Alpha6)) {
            useItem(LootableObject.TypeObject.INVISIBLE);
        }
    }

    public void addItem(LootableObject.TypeObject added) {
        nbObjects[(int)added] += 1;
    }

    public void removeItem(LootableObject.TypeObject removed) {
        nbObjects[(int)removed] -= 1;
    }

    public bool itemAvailable(LootableObject.TypeObject checkItem) {
        if (nbObjects[(int)checkItem] > 0) {
            if (timerObjects[(int)checkItem] == 0f)
                return true;
        }
        return false;
    }

    public void useItem(LootableObject.TypeObject used) {
        if (itemAvailable(used)) {
            timerObjects[(int)used] = baseCDObjects[(int)used];
            switch (used) {
                case LootableObject.TypeObject.LIFE:
                    player.playerStats.changeHP(30);
                    break;
                case LootableObject.TypeObject.MAXLIFE:
                    player.playerStats.changeMaxHP(50);
                    break;
                case LootableObject.TypeObject.STAMINA:
                    player.playerStats.changeStamina(30);
                    break;
                case LootableObject.TypeObject.MAXSTAMINA:
                    player.playerStats.changeMaxStamina(50);
                    break;
                case LootableObject.TypeObject.BERZERK:
                    StartCoroutine(player.playerStats.berzerkMode());
                    break;
                case LootableObject.TypeObject.INVISIBLE:
                    StartCoroutine(player.playerStats.invisibleMode());
                    break;
            }
            removeItem(used);
        }
    }
}
