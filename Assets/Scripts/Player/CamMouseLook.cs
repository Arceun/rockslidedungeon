﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMouseLook: MonoBehaviour {

    Vector2 mouseLook;
    Vector2 smoothV;

    public Parchment story;
    public Quaternion rotationAtStart;
    private float startRotationAngle;

    GameObject character;

    private bool firstUpdate = true;

	// Use this for initialization
	void Start () {
        Debug.Log(rotationAtStart);
        character = transform.parent.gameObject;
        character.transform.localRotation = rotationAtStart;
        //Vector3 axis = character.transform.up;
        //rotationAtStart.ToAngleAxis(out startRotationAngle, out axis);
        //mouseLook.x = startRotationAngle;
    }

    // Update is called once per frame
    void Update () {
        if (!story.gameObject.activeSelf && Time.timeScale == 1) {
            if (Cursor.lockState == CursorLockMode.Locked) {
                var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

                md = Vector2.Scale(md, new Vector2(Settings.sensitivity * Settings.smoothing, Settings.sensitivity * Settings.smoothing));
                smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / Settings.smoothing);
                smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / Settings.smoothing);
                mouseLook += smoothV;

                mouseLook.y = Mathf.Clamp(mouseLook.y, -45, 15);

                transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
            }
        }
    }

    public void replaceCam() {
        Vector3 axis = character.transform.up;
        rotationAtStart.ToAngleAxis(out startRotationAngle, out axis);
        mouseLook.x = startRotationAngle;
    }
}
