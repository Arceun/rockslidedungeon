﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parchment : MonoBehaviour {

    private void Start() {
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Return)) {
            gameObject.SetActive(false);
            Time.timeScale = 1;
            Camera.main.gameObject.GetComponent<CamMouseLook>().replaceCam();
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
