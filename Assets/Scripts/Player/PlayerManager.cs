﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {

    public GameManager GM;
    public PlayerStats playerStats;
    public Inventory inventory;
    public Animator anim;
    public TriggerZonePlayer triggerAttack;
    public UIDamageDisplayer uidamage;
    public Text openChestMSG;
    public Text lootObjectMSG;
    public Light lightPlayer;
    public Torchelight torch;

    public AudioSource source;
    public AudioClip woosh;
    public AudioClip strike;

    private float timerRegen = 0f;

    public bool isDead = false;
    private bool cheatOn = false;
    private bool inAttack = false;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        isDead = false;
        cheatOn = false;
        inAttack = false;
    }

    // Update is called once per frame
    void Update () {
        timerRegen -= Time.deltaTime;
        timerRegen = Mathf.Clamp(timerRegen, 0f, 5f);

        if (!isDead && playerStats.HP <= 0) {
            setToDeath();
            return;
        }
        if (isDead) return;

        if (playerStats.isRunning)
            playerStats.changeStamina(-playerStats.StamSprint5 * Time.deltaTime);
        else
            playerStats.regenStaminaOverTime();
        if (timerRegen == 0f)
            playerStats.regenHPOverTime();

        if (triggerAttack.chest != null && !triggerAttack.chest.isOpen)
            openChestMSG.gameObject.SetActive(true);
        else
            openChestMSG.gameObject.SetActive(false);

        if (triggerAttack.loot != null)
            lootObjectMSG.gameObject.SetActive(true);
        else
            lootObjectMSG.gameObject.SetActive(false);


        if (Input.GetKeyDown(KeyCode.F)) {
            if (triggerAttack.chest != null && !triggerAttack.chest.isOpen)
                triggerAttack.chest.openChest();

            if (triggerAttack.loot != null)
                triggerAttack.loot.lootItem();
        }

        for (var i = triggerAttack.targets.Count - 1; i > -1; i--) {
            if (triggerAttack.targets[i] == null)
                triggerAttack.targets.RemoveAt(i);
        }

        

        if (!inAttack) {
            if (Input.GetMouseButtonUp(0)) {
                anim.SetBool("isIdle", true);
                anim.SetBool("isAttacking", false);
                anim.SetBool("isMoving", false);
            } else if (Time.timeScale == 1 && !anim.GetBool("isAttacking") && Input.GetMouseButtonDown(0) && playerStats.Stam >= playerStats.StamAttack) {
                source.PlayOneShot(woosh);
                anim.SetBool("isAttacking", true);
                anim.SetBool("isIdle", false);
                anim.SetBool("isMoving", false);
                inAttack = true;
                playerStats.changeStamina(-playerStats.StamAttack);
                bool striked = false; 
                if (triggerAttack.targets.Count != 0) {
                    foreach (Enemy go in triggerAttack.targets) {
                        if (!go.dead) {
                            go.takeDamage(playerStats.Damage);
                            timerRegen = 5f;
                            striked = true;
                        }
                    }
                }
                if (striked) source.PlayOneShot(strike);
            } else if (playerStats.isMoving) {
                anim.SetBool("isMoving", true);
                anim.SetBool("isAttacking", false);
                anim.SetBool("isIdle", false);
            } else {
                anim.SetBool("isIdle", true);
                anim.SetBool("isAttacking", false);
                anim.SetBool("isMoving", false);
            }
        }

    }

    public void disableTorch() {
        torch.turnOutTorch();
    }

    public void enableTorch() {
        torch.turnOnTorch();
    }

    public void setToDeath() {
        isDead = true;
        uidamage.displayDead(GM.nbGem);
    }

    public void takeDamage(float dmg, Transform enemy) {
        var relativePoint = transform.InverseTransformPoint(enemy.position);
        if (relativePoint.x < 0.0)
            StartCoroutine(uidamage.displayLeft());
        else if (relativePoint.x > 0.0)
            StartCoroutine(uidamage.displayRight());
        if (relativePoint.z > 0.0)
            StartCoroutine(uidamage.displayFront());
        else if (relativePoint.z < 0.0)
            StartCoroutine(uidamage.displayBack());
        timerRegen = 5f;
        playerStats.changeHP(-dmg);
    }

    public void endAttackAnim() {
        StartCoroutine(enableAttackAfterFewTime());
    }

    private IEnumerator enableAttackAfterFewTime() {
        yield return new WaitForSeconds(0.2f);
        inAttack = false;
    }

    public void toggleCheat() {
        if (cheatOn)
            disableCheat();
        else
            enableCheat();
    }

    private void enableCheat() {
        StartCoroutine(toggleVisionAfterSound());
        cheatOn = true;
    }

    private void disableCheat() {
        StartCoroutine(toggleVisionAfterSound());
        cheatOn = false;
    }

    private IEnumerator toggleVisionAfterSound() {
        lightPlayer.gameObject.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(1.5f);
        lightPlayer.enabled = !lightPlayer.enabled;
        torch.gameObject.SetActive(!torch.gameObject.activeSelf);
    }
}
