﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    public Slider HPBar;
    public Slider StamBar;

    public UIDamageDisplayer uidamage;

    public bool isVisible = true;

    public float Damage;
    public float baseDamage = 30;

    public float MaxHP = 100;
    public float MaxStam = 100;

    public float Hp5 = 10;
    public float Stam5 = 10;

    public float StamSprint5 = 25;
    public float StamAttack = 20;

    public float HP;
    public float Stam;

    public bool isRunning = false;
    public bool isMoving = false;

    // Use this for initialization
    void Start () {
        HPBar.maxValue = MaxHP;
        StamBar.maxValue = MaxStam;
        Damage = baseDamage;

        HP = MaxHP;
        Stam = MaxStam;
	}
	
	// Update is called once per frame
	void Update () {
        StamBar.value = Stam;
        HPBar.value = HP;
	}

    public void changeHP(float modifier) {
        HP += modifier;
        HP = Mathf.Clamp(HP, 0, MaxHP);
    }

    public void changeStamina(float modifier) {
        Stam += modifier;
        Stam = Mathf.Clamp(Stam, 0, MaxStam);
    }

    public void regenHPOverTime() {
        HP += ((Hp5) * Time.deltaTime);
        HP = Mathf.Clamp(HP, 0, MaxHP);
    }

    public void regenStaminaOverTime() {
        Stam += ((Stam5) * Time.deltaTime);
        Stam = Mathf.Clamp(Stam, 0, MaxStam);
    }

    public void changeMaxHP(float modifier) {
        MaxHP += modifier;
        HPBar.maxValue = MaxHP;
    }

    public void changeMaxStamina(float modifier) {
        MaxStam += modifier;
        StamBar.maxValue = MaxStam;
    }

    public IEnumerator berzerkMode() {
        uidamage.berzerkMode.gameObject.SetActive(true);
        Damage *= 2;
        yield return new WaitForSeconds(15f);
        Damage /= 2;
        uidamage.berzerkMode.gameObject.SetActive(false);
    }

    public IEnumerator invisibleMode() {
        uidamage.invisibleMode.gameObject.SetActive(true);
        isVisible = false;
        yield return new WaitForSeconds(10f);
        isVisible = true;
        uidamage.invisibleMode.gameObject.SetActive(false);
    }
}
