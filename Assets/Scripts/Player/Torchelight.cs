using UnityEngine;
using System.Collections;

public class Torchelight : MonoBehaviour {
	
	public GameObject TorchLight;
	public GameObject MainFlame;
	public GameObject BaseFlame;
	public GameObject Etincelles;
	public GameObject Fumee;
	public float MaxLightIntensity;
	public float IntensityLight;

    private bool isEnable = true;

	void Start () {
		TorchLight.GetComponent<Light>().intensity=IntensityLight;
		MainFlame.GetComponent<ParticleSystem>().emissionRate=IntensityLight*20f;
		BaseFlame.GetComponent<ParticleSystem>().emissionRate=IntensityLight*15f;	
		Etincelles.GetComponent<ParticleSystem>().emissionRate=IntensityLight*7f;
		Fumee.GetComponent<ParticleSystem>().emissionRate=IntensityLight*12f;
	}
	

	void Update () {
        if (isEnable) {
            if (IntensityLight < 0) IntensityLight = 0;
            if (IntensityLight > MaxLightIntensity) IntensityLight = MaxLightIntensity;

            TorchLight.GetComponent<Light>().intensity = IntensityLight / 2f + Mathf.Lerp(IntensityLight - 0.1f, IntensityLight + 0.1f, Mathf.Cos(Time.time * 30));

            MainFlame.GetComponent<ParticleSystem>().emissionRate = IntensityLight * 20f;
            BaseFlame.GetComponent<ParticleSystem>().emissionRate = IntensityLight * 15f;
            Etincelles.GetComponent<ParticleSystem>().emissionRate = IntensityLight * 7f;
            Fumee.GetComponent<ParticleSystem>().emissionRate = IntensityLight * 12f;
        }
	}

    private void FadeLight(float fadeStart, float fadeEnd, float fadeTime) {
        float t = 0.0f;
        Light light = TorchLight.GetComponent<Light>();
        while (t < fadeTime) {
            t += Time.deltaTime;
            light.intensity = Mathf.Lerp(fadeStart, fadeEnd, t / fadeTime);
        }
    }

    public void turnOnTorch() {
        isEnable = true;
        FadeLight(0f, IntensityLight, 1f);
        MainFlame.GetComponent<ParticleSystem>().Play();
        BaseFlame.GetComponent<ParticleSystem>().Play();
        Etincelles.GetComponent<ParticleSystem>().Play();
        Fumee.GetComponent<ParticleSystem>().Play();
    }

    public void turnOutTorch() {
        isEnable = false;
        MainFlame.GetComponent<ParticleSystem>().Stop();
        BaseFlame.GetComponent<ParticleSystem>().Stop();
        Etincelles.GetComponent<ParticleSystem>().Stop();
        Fumee.GetComponent<ParticleSystem>().Stop();
        FadeLight(TorchLight.GetComponent<Light>().intensity, 0f, 1f);
    }

}
