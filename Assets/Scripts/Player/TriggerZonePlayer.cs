﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerZonePlayer : MonoBehaviour {

    public List<Enemy> targets = new List<Enemy>();

    public Chest chest = null;

    public LootableObject loot = null;

	// Use this for initialization
	void Start () {
	}

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Enemy")) {
            Enemy go = other.GetComponent<Enemy>();
            if (!targets.Contains(go)) {
                targets.Add(go);
            }
        } else if (other.CompareTag("Chest")) {
            chest = other.GetComponent<Chest>();
        } else if (other.CompareTag("Loot")) {
            loot = other.GetComponentInParent<LootableObject>();
            Debug.Log("Lootable entered zone");
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Enemy")) {
            Enemy go = other.GetComponent<Enemy>();
            targets.Remove(go);
        } else if (other.CompareTag("Chest")) {
            chest = null;
        } else if (other.CompareTag("Loot")) {
            loot = null;
            Debug.Log("Lootable leaved zone");
        }
    }
}
