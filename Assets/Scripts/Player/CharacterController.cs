﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public float walking;
    public float running;

    public Parchment story;

    public PlayerManager player;

    private void Start() {
    }

    private void Update() {
        if (!story.gameObject.activeSelf && !player.isDead) {
            if (Cursor.lockState == CursorLockMode.Locked) {

                if (player.playerStats.isRunning && Input.GetKey(KeyCode.LeftShift) && player.playerStats.Stam >= (player.playerStats.StamSprint5 * Time.deltaTime)) {
                    player.playerStats.isRunning = true;
                    player.playerStats.changeStamina(-(player.playerStats.StamSprint5 * Time.deltaTime));
                } else if (Input.GetKey(KeyCode.LeftShift) && player.playerStats.Stam >= 30) {
                    player.playerStats.isRunning = true;
                    player.playerStats.changeStamina(-(player.playerStats.StamSprint5 * Time.deltaTime));
                } else {
                    player.playerStats.isRunning = false;
                }

                float translation = Input.GetAxis("Vertical") * ((player.playerStats.isRunning) ? running : walking);
                float straf = Input.GetAxis("Horizontal") * ((player.playerStats.isRunning) ? running : walking);

                translation *= Time.deltaTime;
                straf *= Time.deltaTime;

                if (translation != 0 || straf != 0)
                    player.playerStats.isMoving = true;
                else
                    player.playerStats.isMoving = false;

                gameObject.GetComponent<Rigidbody>().velocity = (transform.forward * translation) + (transform.right * straf);
            }
        }
    }

}
