﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public Maze mazePrefab;
	public float scale;
    public Parchment story;
    public GameObject player;
    public PlayerStats playerStats;

    private Maze mazeInstance;
    private Animator anim;

    private bool inAttack = false;

    void Start () {
		BeginGame();
        anim = player.GetComponent<Animator>();
	}

    void Update() {
        if (!story.gameObject.activeSelf) {

            if (playerStats.isRunning)
                playerStats.changeStamina(-playerStats.StamSprint5 * Time.deltaTime);
            else
                playerStats.regenStaminaOverTime();
            playerStats.regenHPOverTime();
            playerStats.regenMPOverTime();

            if (Input.GetKeyDown(KeyCode.F7)) {
                RestartGame();
            }
            if (Input.GetKeyDown(KeyCode.Escape)) {
                SceneLoader.loadScene(SceneLoader.START_MENU);
            }

            if (!inAttack) {
                if (/*anim.GetBool("isAttacking") && */Input.GetKeyUp(KeyCode.Space)) {
                    anim.SetBool("isIdle", true);
                    anim.SetBool("isAttacking", false);
                    anim.SetBool("isMoving", false);
                } else if (!anim.GetBool("isAttacking") && Input.GetKeyDown(KeyCode.Space) && playerStats.Stam >= playerStats.StamAttack) {
                    anim.SetBool("isAttacking", true);
                    anim.SetBool("isIdle", false);
                    anim.SetBool("isMoving", false);
                    playerStats.changeStamina(-playerStats.StamAttack);
                } else if (playerStats.isMoving) {
                    anim.SetBool("isMoving", true);
                    anim.SetBool("isAttacking", false);
                    anim.SetBool("isIdle", false);
                } else {
                    anim.SetBool("isIdle", true);
                    anim.SetBool("isAttacking", false);
                    anim.SetBool("isMoving", false);
                }
            }
        }
    }

    private void BeginGame() {
		mazeInstance = Instantiate(mazePrefab) as Maze;
		mazeInstance.Generate();
		mazeInstance.transform.localScale = new Vector3(scale, scale, scale);
	}

	private void RestartGame() {
		Destroy(mazeInstance.gameObject);
		BeginGame();
	}
}
